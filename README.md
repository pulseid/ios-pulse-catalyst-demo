# Pulse Catalyst iOS Demo

This is a simple working demo app that utilizes our Android SDK. The complete Android documentation can be found here.

## Quickstart

To run this application and load offers, please configure below values in `AppCoordinator.swift`

```swift
let offerURL: String = "<PULSE_OFFER_URL>"
let storyURL: String = "<PULSE_STORY_URL>"
```

`AppCoordinator.swift` `storeSecrets` method.

```swift
let apiKey = "<YOUR_PULSE_API_KEY>"
let apiSecret = "<YOUR_PULSE_API_SECRET>"
```

Please refer to the documentation here for integration steps.

http://pulse-test-support.pulseid.com/code-snippet/ios/index.html

## Support

In case you encounter any issues please email: support@pulseid.com