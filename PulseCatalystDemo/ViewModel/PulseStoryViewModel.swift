import Foundation

protocol PulseStoryViewModelProtocol {
    var offerId: Int { get }
    var storyURL: URL { get }
    var config: PulseViewConfig { get }
    var headers: PulseAuthHeader { get }
}

struct PulseStoryViewModel: PulseStoryViewModelProtocol {
    var offerId: Int
    var storyURL: URL
    var config: PulseViewConfig

    private var apiKey: String
    private var apiSecret: String
    private var userId: String

    var headers: PulseAuthHeader {
        return PulseAuthHeader(apiKey: apiKey, apiSecret: apiSecret, userId: userId)
    }

    init?(offerId: Int,
          storyURL: String,
          apiKey: String,
          apiSecret: String,
          userId: String,
          config: PulseViewConfig) {
        self.offerId = offerId
        self.config = config
        if let url = URL(string: "\(storyURL)?offer_id=\(offerId)") {
            self.storyURL = url
        } else {
            return nil
        }
        self.apiKey = apiKey
        self.apiSecret = apiSecret
        self.userId = userId
    }
    
}
