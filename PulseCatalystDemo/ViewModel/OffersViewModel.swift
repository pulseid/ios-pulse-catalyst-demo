import Foundation

protocol OffersViewModelProtocol {
    var apiKey: String { get }
    var apiSecret: String { get }
    var userId: String { get }
    var offersURL: URL { get }
    var config: PulseViewConfig? { get }
}

struct OffersViewModel: OffersViewModelProtocol {
    var apiKey: String
    var apiSecret: String
    var userId: String
    var offersURL: URL
    var config: PulseViewConfig?
    
    init?(offersURL: String,
          apiKey: String,
          apiSecret: String,
          userId: String,
          config: PulseViewConfig? = nil) {
        self.config = config
        if let url = URL(string: offersURL) {
            self.offersURL = url
        } else {
            return nil
        }
        self.apiKey = apiKey
        self.apiSecret = apiSecret
        self.userId = userId
    }
    
}
