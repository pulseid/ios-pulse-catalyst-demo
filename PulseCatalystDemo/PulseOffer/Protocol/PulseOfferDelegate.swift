import Foundation

public protocol PulseOfferDelegate: AnyObject {
    /// Called on error received while loading Pulse Offer
    /// - Parameter error: Error returened by WKWebView
    func didReceiveError(_ error: Error)
    
    /// Called when offers finished loading and pulseview show offer on screen
    /// ActivityIndicator will be hidden, if enabled.
    func didFinishOffersLoading()
    
    /// Called when offers starts loading.
    func didStartOffersLoading()
    
    /// Called if webview return some error while applying PulseOfferConfig using evaulateJavascript.
    /// - Parameter error: Error received from evaluateJavascript
    func failedToApplyConfig(with error: Error)
    
    /// Called when some events occured on PulseOfferView. Use this to perform task on particular event.
    /// - Parameter event: <#event description#>
    func receive(event: PulseOfferEvent)

}

extension PulseOfferDelegate {
    func didReceiveError(_ error: Error) {}
    func didFinishOffersLoading() {}
    func didStartOffersLoading() {}
    func failedToApplyConfig(with error: Error) {}
    func receive(event: PulseOfferEvent) {}
}
