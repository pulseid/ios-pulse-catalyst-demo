import Foundation
import UIKit

class PulseOffersErrorView: UIView {
    
    private var reloadAction: (() -> Void)?
    private var title: String?
    private var message: String?
    
    convenience init(onReload: (() -> Void)?,
                     title: String? = nil,
                     message: String? = nil) {
        self.init(frame: .zero)
        self.reloadAction = onReload
        self.title = title
        self.message = message
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(stackView)
        backgroundColor = UIColor(red: 0.949, green: 0.953, blue: 0.957, alpha: 1.0)
        layer.cornerRadius = 5.0
        clipsToBounds = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .center
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        label.text = NSLocalizedString(title ?? "Offer Carousell Isn't Available Right Now",
                                       comment: "Offers Error Title")
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.textColor = .lightGray
        label.textAlignment = .center
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        label.text = NSLocalizedString(message ?? "This maybe because of a technical error that we're working to get fixed. Try reloading the page.",
                                       comment: "Offers Error Message")
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var reloadButton: UIButton = {
        let button = UIButton()
        button.setTitle(NSLocalizedString("Try again", comment: "Reload Button"), for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13.0)
        button.addTarget(self, action: #selector(reloadButtonTapped), for: .touchUpInside)
        button.backgroundColor = .clear
        button.setTitleColor(.black, for: .normal)
        button.layer.borderColor = UIColor(red: 0.235, green: 0.235, blue: 0.263, alpha: 1.0).cgColor
        button.layer.borderWidth = 2.0
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    private lazy var buttonSuperView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.addSubview(reloadButton)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var carousellImageView: UIImageView = {
        guard let image = UIImage(named: "icon-empty-state-carousell") else {
            fatalError("Please add carousell error state image")
        }
        let imageView = UIImageView(image: image)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .top
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            titleLabel,
            messageLabel,
            buttonSuperView,
            carousellImageView
        ])
        stackView.axis = .vertical
        stackView.spacing = 5.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution = .fill
        
        return stackView
    }()
    
    @objc private func reloadButtonTapped() {
        reloadAction?()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        stackView.topAnchor.constraint(equalTo: topAnchor, constant: 20).isActive = true
        stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8.0).isActive = true
        stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8.0).isActive = true
        stackView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        titleLabel.heightAnchor.constraint(equalToConstant: 22.0).isActive = true
        messageLabel.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        buttonSuperView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        reloadButton.centerXAnchor.constraint(equalTo: buttonSuperView.centerXAnchor).isActive = true
        reloadButton.topAnchor.constraint(equalTo: buttonSuperView.topAnchor).isActive = true
        reloadButton.bottomAnchor.constraint(equalTo: buttonSuperView.bottomAnchor).isActive = true
        reloadButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        reloadButton.layer.cornerRadius = reloadButton.bounds.height * 0.5
    }
}

class PulseStoryErrorView: UIView {
    
}
