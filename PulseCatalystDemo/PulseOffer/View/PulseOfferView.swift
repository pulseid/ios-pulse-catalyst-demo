import Foundation
import WebKit

public class PulseOfferView: WKWebView {

    enum Mode {
        case carousel
        case story

        var htmlFileName: String {
            switch self {
            case .carousel:
                return "error_carousel"
            case .story:
                return "error_story"
            }
        }
    }

    public weak var pulseOffersDelegate: PulseOfferDelegate?
    private var config: PulseViewConfig!
    private var authHeader: PulseAuthHeader!
    private var mode: Mode = .carousel
    private var pulseURL: URL?

    public var transparentBackground: Bool {
        @available(*, unavailable)
        get {
            fatalError("You cannot read from this property.")
        }
        set {
            if newValue {
                self.backgroundColor = .clear
                self.scrollView.backgroundColor = .clear
                self.isOpaque = false
            }
        }
    }

    public var isActivityIndicatorEnabled: Bool = true {
        didSet {
            activityIndicatorView.isHidden = !isActivityIndicatorEnabled
        }
    }

    lazy var activityIndicatorView: UIActivityIndicatorView = {
        var indicator: UIActivityIndicatorView
        if #available(iOS 13.0, *) {
            indicator = UIActivityIndicatorView(style: .large)
        } else {
            indicator = UIActivityIndicatorView(style: .gray)
        }
        indicator.color = isDarkMode ? .white : .gray
        indicator.translatesAutoresizingMaskIntoConstraints = false
        return indicator
    }()

    convenience init(_ authHeader: PulseAuthHeader,
                     config: PulseViewConfig? = nil) {
        let configuration = WKWebViewConfiguration()
        self.init(frame: .zero, configuration: configuration)
        self.config = config ?? PulseViewConfig()
        self.authHeader = authHeader
    }

    private override init(frame: CGRect, configuration: WKWebViewConfiguration) {
        let preferences = WKPreferences()
        preferences.setValue(true, forKey: "developerExtrasEnabled")
        // iOS 14.7.1 require allowsContentJavascript to execute inline JS script
        configuration.preferences = preferences
        configuration.applicationNameForUserAgent = PulseOfferView.userAgentString()

        super.init(frame: frame, configuration: configuration)

        navigationDelegate = self
        scrollView.delegate = self
        scrollView.bounces = false
        allowsLinkPreview = false
        scrollView.showsVerticalScrollIndicator = false
        let contentController = self.configuration.userContentController
        contentController.add(self, name: "PulseiD")

        addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func load(_ request: URLRequest) -> WKNavigation? {
        pulseURL = request.url
        if pulseURL?.absoluteString.contains("story") ?? false {
            mode = .story
        }
        let pulseRequest = setPulseHeaders(request)

        return super.load(pulseRequest)
    }
    
    public func configure(authHeader: PulseAuthHeader,
                   config: PulseViewConfig? = nil) {
        self.authHeader = authHeader
        self.config = config

        self.reloadWebURL()
    }

    private func setPulseHeaders(_ request: URLRequest) -> URLRequest {
        var updatedRequest = request
        _ = authHeader.headersDictionary.map {
            key, value in
            updatedRequest.setValue(value, forHTTPHeaderField: key)
        }

        return updatedRequest
    }

    private func configureView(with config: PulseViewConfig) {
        do {
            let configHeader = Config(config: config)
            let configString = try configHeader.jsonString() ?? "{}"
            print(configString)
            let js = "renderApp(\(configString));"
            self.evaluateJavaScript(js) { [weak self] result, error in
                if let error = error {
                    self?.pulseOffersDelegate?.failedToApplyConfig(with: error)
                }
            }
        } catch let error {
            pulseOffersDelegate?.failedToApplyConfig(with: error)
        }
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        activityIndicatorView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        activityIndicatorView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }

    private static func userAgentString() -> String {
        let bundleIdentifier = Bundle.main.bundleIdentifier ?? "CatalystStory"
        let version = getAppVersion()

        return "\(bundleIdentifier)/\(version)"
    }

    private static func getAppVersion() -> String {
        let dictionary = Bundle.main.infoDictionary!
        let versionValue = dictionary["CFBundleShortVersionString"] ?? "0"
        let buildValue = dictionary["CFBundleVersion"] ?? "0"
        return "\(versionValue).\(buildValue))"
    }

    private func reloadWebURL() {
        guard let url = pulseURL else {
            print("Failed to reload, no url set.")
            return
        }
        let request = URLRequest(url: url)
        _ = load(request)
        activityIndicatorView.startAnimating()
    }

    private func showErrorView() {
        guard let errorHTMLURL = Bundle.main.url(forResource: mode.htmlFileName, withExtension: ".html") else {
            return
        }
        loadFileURL(errorHTMLURL, allowingReadAccessTo: errorHTMLURL.deletingLastPathComponent())
    }
}

extension PulseOfferView: WKNavigationDelegate {
    public func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        pulseOffersDelegate?.didReceiveError(error)
        activityIndicatorView.stopAnimating()
    }
    
    public func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        pulseOffersDelegate?.didReceiveError(error)
        activityIndicatorView.stopAnimating()
        self.showErrorView()
    }
    
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        configureView(with: config)
        pulseOffersDelegate?.didFinishOffersLoading()
        activityIndicatorView.stopAnimating()
    }
    
    public func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        pulseOffersDelegate?.didStartOffersLoading()
    }

    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        print(navigationAction)
        guard let url = navigationAction.request.url else {
            decisionHandler(.cancel)
            return
        }
        if url.absoluteString.contains("carousel.html") || url.absoluteString.contains("story.html") {
            decisionHandler(.allow)
            return
        } else if let termsAndConditionURL = config.generalTermsAndConditionsUrl, url.absoluteString.contains(termsAndConditionURL) {
            pulseOffersDelegate?.receive(event: .termsAndConditionTapped(url: url))
            decisionHandler(.cancel)
            return
        }
        decisionHandler(.cancel)
    }

}

extension PulseOfferView: WKScriptMessageHandler{
    public func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        guard let dict = message.body as? [String : AnyObject] else {
            return
        }
        if let offerId = dict["offer_id"] as? Int {
            pulseOffersDelegate?.receive(event: .viewOffer(offerId: offerId))
        } else if let action = dict["action"] as? String {
            if action == "story_view_close_tapped" {
                pulseOffersDelegate?.receive(event: .closeStory)
            } else if action == "reload_page" {
                self.reloadWebURL()
            }
        }
        print(dict)
    }
}

extension PulseOfferView: UIScrollViewDelegate {
    // Disable pinch to zoom on offer screens
    public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return nil
    }
}

extension PulseOfferView {
    // Refresh Carousel view to reflect latest and subscribed offers
    public func refreshCarousel() {
        if pulseURL?.absoluteString.contains("carousel") ?? false {
            self.evaluateJavaScript("refetchOfferAttribution()") { [weak self] result, error in
                if let error = error {
                    self?.pulseOffersDelegate?.failedToApplyConfig(with: error)
                }
            }
        }
    }
}

extension UIView {
    var isDarkMode: Bool {
        if #available(iOS 13.0, *) {
            return self.traitCollection.userInterfaceStyle == .dark
        }
        else {
            return false
        }
    }
}

