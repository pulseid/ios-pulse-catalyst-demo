import Foundation

public struct PulseViewConfig: Encodable {
    var backgroundColor: String?
    var borderColor: String?
    var title: String?
    var subTitle: String?
    var generalTermsAndConditionsUrl: String?
    var generalTermsAndConditionsLinkText: String?
    var carouselPaddingLeft: Float = 20.0
    var offersPerPage: Int = 1
    var story: StoryConfig = StoryConfig()

    public struct StoryConfig: Encodable {
        var duration: Int = 4000
        var ctaText: String = "Activate Offer"
        var activatedCtaText: String = "Activated"
        var successPopupHeading: String = "Activated"
        var successPopupMessage: String = "You have activated offer on your card successfully. Transact using your card to redeem offer."

        public init() {
        }

        public init(duration: Int = 4000,
             ctaText: String = "Activate Offer",
             activatedCtaText: String = "Activated",
             successPopupHeading: String = "Activated",
             successPopupMessage: String = "You have activated offer on your card successfully. Transact using your card to redeem offer.") {
            self.duration = duration
            self.ctaText = ctaText
            self.activatedCtaText = activatedCtaText
            self.successPopupHeading = successPopupHeading
            self.successPopupMessage = successPopupMessage
        }
    }

    public init() {
    }

    public init(backgroundColor: String? = nil,
         borderColor: String? = nil,
         title: String? = nil,
         subTitle: String? = nil,
         generalTermsAndConditionsUrl: String? = nil,
         generalTermsAndConditionsLinkText: String? = nil,
         carouselPaddingLeft: Float = 20.0,
         offersPerPage: Int = 10,
         storyConfig: StoryConfig = StoryConfig()) {
        self.backgroundColor = backgroundColor
        self.borderColor = borderColor
        self.title = title
        self.subTitle = subTitle
        self.generalTermsAndConditionsUrl = generalTermsAndConditionsUrl
        self.generalTermsAndConditionsLinkText = generalTermsAndConditionsLinkText
        self.carouselPaddingLeft = carouselPaddingLeft
        self.offersPerPage = offersPerPage
        self.story = storyConfig
    }
}

struct Config: Encodable {
    var config: PulseViewConfig

    func jsonString() throws -> String?  {
        let encoder = JSONEncoder()
        let data = try encoder.encode(self)
        return String(data: data, encoding: .utf8)
    }
}
