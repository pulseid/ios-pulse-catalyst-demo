import Foundation

public enum PulseOfferEvent {
    // Event for offer tapped on carousel, offerId of tapped Offer
    case viewOffer(offerId: Int)

    // Event for closing OfferStoryView
    case closeStory

    // Terms And condition URL tapped
    case termsAndConditionTapped(url: URL)
}
