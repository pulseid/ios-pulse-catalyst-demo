import Foundation

public struct PulseAuthHeader {
    
    var apiKey: String
    var apiSecret: String
    var userId: String
    var traceId: String = UUID().uuidString
    
    var headersDictionary: [String: String] {
        var dict: [String: String] = [:]
        dict["x-api-key"] = apiKey
        dict["x-api-secret"] = apiSecret
        dict["euid"] = userId
        dict["trace-id"] = traceId
        
        return dict
    }

    public init(apiKey: String, apiSecret: String, userId: String, traceId: String = UUID().uuidString) {
        self.apiKey = apiKey
        self.apiSecret = apiSecret
        self.userId = userId
        self.traceId = traceId
    }
    
}
