
import UIKit

class PulseStoryViewController: UIViewController {
    var viewModel: PulseStoryViewModelProtocol
    
    lazy var pulseOffersView: PulseOfferView = {
        let offersView = PulseOfferView(viewModel.headers, config: viewModel.config)
        offersView.pulseOffersDelegate = self
        offersView.translatesAutoresizingMaskIntoConstraints = false
        offersView.transparentBackground = true
        offersView.isActivityIndicatorEnabled = true
        return offersView
    }()
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    init(viewModel: PulseStoryViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupPulseView()
        navigationController?.navigationBar.isHidden = true
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
    }
    
    private func setupPulseView() {
        view.addSubview(pulseOffersView)
        let request = URLRequest(url: viewModel.storyURL)
        _ = pulseOffersView.load(request)
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        pulseOffersView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        pulseOffersView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        pulseOffersView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        pulseOffersView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
}

extension PulseStoryViewController: PulseOfferDelegate {
    func receive(event: PulseOfferEvent) {
        print("Event received \(event)")
        switch event {
        case .closeStory:
            dismiss(animated: true, completion: {
                // Do something
            })
        case .termsAndConditionTapped(let url):
            let termsAndConditionViewController = PulseTermsAndConditionViewController(
                title: viewModel.config.generalTermsAndConditionsLinkText ?? "Terms & Conditions",
                termsAndConditionURL: url)
            let navigationController = UINavigationController(rootViewController: termsAndConditionViewController)
            present(navigationController, animated: true, completion: nil)
        default:
            break
        }
    }
    
    func didReceiveError(_ error: Error) {
        print(error)
    }
    
    func didFinishOffersLoading() {
        print("Finished Pulse Offers Loading")
    }
    
    func didStartOffersLoading() {
        print("Started Offers loading")
    }
    
    func failedToApplyConfig(with error: Error) {
        print("Config apply failed")
    }
}
