import WebKit
import UIKit

class PulseTermsAndConditionViewController: UIViewController {
    var termsAndConditionURL: URL
    var termsAndConditionTitle: String

    lazy var webView: WKWebView = {
        let webView = WKWebView()
        webView.translatesAutoresizingMaskIntoConstraints = false
        return webView
    }()

    init(title: String, termsAndConditionURL: URL) {
        self.termsAndConditionTitle = title
        self.termsAndConditionURL = termsAndConditionURL
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.termsAndConditionTitle

        if #available(iOS 13.0, *) {
            navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .close, target: self, action: #selector(closeTapped))
        } else {
            navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(closeTapped))
        }

        setupWebView()
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
    }

    private func setupWebView() {
        view.addSubview(webView)
        let request = URLRequest(url: termsAndConditionURL)
        _ = webView.load(request)
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        webView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        webView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        webView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }

    @objc func closeTapped() {
        dismiss(animated: true, completion: nil)
    }
}

