import UIKit

class OffersViewController: UIViewController {
    
    var viewModel: OffersViewModelProtocol
    var onOfferSelected: ((Int) -> ())?
    
    lazy var pulseOfferView: PulseOfferView = {
        let authHeader = PulseAuthHeader(apiKey: viewModel.apiKey, apiSecret: viewModel.apiSecret, userId: viewModel.userId)
        let offersView = PulseOfferView(authHeader, config: viewModel.config)
        offersView.pulseOffersDelegate = self
        offersView.translatesAutoresizingMaskIntoConstraints = false
        offersView.transparentBackground = true
        return offersView
    }()
    
    init(viewModel: OffersViewModelProtocol, onOfferSelected: ((Int) -> ())?) {
        self.viewModel = viewModel
        self.onOfferSelected = onOfferSelected
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Catalyst Demo"
        
        setupPulseView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = false
        pulseOfferView.refreshCarousel()
    }
    
    private func setupPulseView() {
        view.addSubview(pulseOfferView)
        let request = URLRequest(url: viewModel.offersURL)
        _ = pulseOfferView.load(request)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        pulseOfferView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20.0).isActive = true
        pulseOfferView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        pulseOfferView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        pulseOfferView.heightAnchor.constraint(equalToConstant: 250).isActive = true
    }
}

extension OffersViewController: PulseOfferDelegate {
    func receive(event: PulseOfferEvent) {
        print("Event received \(event)")
        switch event {
            case .viewOffer(let offerId):
                onOfferSelected?(offerId)
            default:
                break
        }
    }
    
    func didReceiveError(_ error: Error) {
        print(error)
    }
    
    func didFinishOffersLoading() {
        print("Finished Pulse Offers Loading")
    }
    
    func didStartOffersLoading() {
        print("Started Offers loading")
    }
    
    func failedToApplyConfig(with error: Error) {
        print("Config apply failed")
    }
}

