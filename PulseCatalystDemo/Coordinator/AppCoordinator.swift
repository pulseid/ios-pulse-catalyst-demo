import Foundation
import UIKit

let offerURL: String = "<PULSE_OFFER_URL>"
let storyURL: String = "<PULSE_STORY_URL>"

final class AppCoordinator: Coordinator {
    
    let secretStore = AppCoordinator.createKeychainStore()
    var apiKey: String? {
        return try? secretStore.getValue(for: "catalyst.apiKey")
    }
    var apiSecret: String? {
        return try? secretStore.getValue(for: "catalyst.apiSecret")
    }
    let userId = "20211001"
    
    override init(window: UIWindow? = nil,
                  navigationController: UINavigationController? = nil) {
        
        super.init(window: window, navigationController: navigationController)
    }
    
    override func start() {
        childCoordinators = []
        window?.rootViewController = UINavigationController()
        navigationController = window?.rootViewController as? UINavigationController
        try? secretStore.removeAllValues()
        storeSecrets()
        startMainView()
        window?.makeKeyAndVisible()
    }
    
    func startMainView() {
        let config = PulseViewConfig(backgroundColor: "#ac171a",
                                     borderColor: "#ac171a",
                                     title: "Title",
                                     subTitle: "Subtitle",
                                     generalTermsAndConditionsUrl: "https://google.com",
                                     generalTermsAndConditionsLinkText: "Terms and Conditions",
                                     carouselPaddingLeft: 16.0)
        guard let apiKey = apiKey,
              let apiSecret = apiSecret,
              let viewModel = OffersViewModel(offersURL: offerURL,
                                              apiKey: apiKey,
                                              apiSecret: apiSecret,
                                              userId: userId,
                                              config: config) else {
            fatalError("Failed to create viewmodel for offers view")
        }
        let viewController = OffersViewController(viewModel: viewModel) { [weak self] offerId in
            self?.startPulseStoryView(offerId: offerId)
        }
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func startPulseStoryView(offerId: Int) {
        let config = PulseViewConfig(backgroundColor: "#ac171a",
                                     borderColor: "#ac171a",
                                     title: "Title",
                                     subTitle: "Subtitle",
                                     generalTermsAndConditionsUrl: "https://google.com",
                                     generalTermsAndConditionsLinkText: "Terms and Conditions",
                                     carouselPaddingLeft: 16.0)
        guard let apiKey = apiKey,
              let apiSecret = apiSecret,
              let viewModel = PulseStoryViewModel(offerId: offerId,
                                                  storyURL: storyURL,
                                                  apiKey: apiKey,
                                                  apiSecret: apiSecret,
                                                  userId: userId,
                                                  config: config) else {
            fatalError("Failed to create viewmodel for story view")
        }
        let viewController = PulseStoryViewController(viewModel: viewModel)
        navigationController?.present(viewController, animated: true, completion: {
          // Do something
        })
    }
    
    fileprivate static func createKeychainStore() -> KeychainStore {
        let genericSecretQueryable =
            GenericPasswordQueryable(service: "catalyst_story_service")
        return KeychainStore(keychainQueryable: genericSecretQueryable)
    }
    
    fileprivate func storeSecrets() {
        let apiKey = "<YOUR_PULSE_API_KEY>"
        let apiSecret = "<YOUR_PULSE_API_SECRET>"
        do {
            try secretStore.setValue(apiKey, for: "catalyst.apiKey")
            try secretStore.setValue(apiSecret, for: "catalyst.apiSecret")
        } catch let error {
            print(error.localizedDescription)
        }
    }
}
