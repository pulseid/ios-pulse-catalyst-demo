import Foundation
import UIKit

class Coordinator {
    
    var childCoordinators: [Coordinator] = []
    weak var navigationController: UINavigationController?
    weak var window: UIWindow?
    
    init(window: UIWindow? = nil,
         navigationController: UINavigationController? = nil) {
        self.window = window
        self.navigationController = navigationController
        
        if #available(iOS 13.0, *) {
            self.window?.backgroundColor = .systemBackground
        } else {
            self.window?.backgroundColor = .white
        }
    }
    
    func removeCoordinator(_ coordinator: Coordinator) {
        if let index = childCoordinators.firstIndex(where: { $0 === coordinator }) {
            childCoordinators.remove(at: index)
        }
    }
    
    // Override this method in subclass for presenting first View
    func start() {
    }
}
